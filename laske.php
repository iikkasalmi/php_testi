<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>


<h2>Nettopalkka</h2>
    <?php
        $palkka = filter_input(INPUT_POST,'palkka',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $vero = filter_input(INPUT_POST,'vero',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $elake = filter_input(INPUT_POST,'elake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $vakuutus = filter_input(INPUT_POST,'vakuutus',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);

        $nettopalkka = $palkka - ($palkka * (($vero + $elake + $vakuutus) / 100));
        printf("<p>Nettopalkka on %.2f </p>",$nettopalkka);

        $ennakkopidatys = ($palkka * $vero) / 100;
        printf("<p>Ennakonpidätys %.2f € </p>",$ennakkopidatys);

        $elakemaksu = ($palkka * $elake) / 100;
        printf("<p>Eläkemaksu %.2f € </p>",$elakemaksu);

        $vakuutusmaksu = ($palkka * $vakuutus) / 100;
        printf("<p>Työvakuutusmaksu %.2f € </p>",$vakuutusmaksu);

    ?>
    <a href="index.html">Laske uudestaan</a>
</body>
</html>